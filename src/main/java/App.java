import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class App extends Application {

    @Override
    public void start(Stage stage) {
        var label = new Label("Hola mundo");
        label.setVisible(false);
        
        var button = new Button("Saluda");
        button.setOnAction(actionEvent -> {
            button.setVisible(false);
            label.setVisible(true);
        });
        
        var menuBar = new MenuBar();
        var menu = new Menu("Archivo");
        var salir = new MenuItem("Salir");
        salir.setOnAction(actionEvent -> {
            exitApplication(actionEvent);
        });
        menu.getItems().add(salir);
        menuBar.getMenus().add(menu);

        
        var pane = new VBox();
        pane.getChildren().addAll(menuBar, button, label);
        pane.setBackground(new Background(new BackgroundFill(Color.ORANGE, null, null)));
        
        var scene = new Scene(pane, 640, 480);
        
        stage.setScene(scene);
        stage.setTitle("Mi primera aplicación JavaFX");
        
        stage.setOnCloseRequest(windowEvent -> {
            exitApplication(windowEvent);
        });
        
        stage.show();
        
    }

    @Override
    public void init() throws Exception {
        super.init(); //To change body of generated methods, choose Tools | Templates.
    }

    public static void main(String[] args) {
        launch();
    }
    
    private void exitApplication(Event e){
        var alert = new Alert(Alert.AlertType.CONFIRMATION, "¿Estás seguro de que deseas salir?");
            var result = alert.showAndWait();
            if(result.isPresent() && result.get() == ButtonType.OK){
                Platform.exit();
            } else {
                e.consume();
            }
    }
}